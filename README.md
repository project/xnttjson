External Entities JSON Files plugin
***********************************

Enables to manage entities stored in one or more JSON files.

Supported structures:
-a single main file with all entities inside.
-complex directory structure with multiple JSON files:
  A directory pattern is defined based on the entity fields to match the JSON
  file to which the entity belongs to.

===============================

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

This modules provides a way to use JSON files content as Drupal entities. Drupal
sees those entities like regular entities while they are not stored in database
but in JSON file. While this approach is slower than storing entities in
database, it can provide several advantages and can solve some specific use
cases.

Please be aware that this module does not support concurrent file editing. It
means that if several applications (Drupal and non-Drupal) open a JSON file for
editing concurrently, only the last one saving its changes will be recorded.

[JSON file](https://en.wikipedia.org/wiki/JSON)
All entities can be stored in a single JSON as well as in separated JSON. In
order to use multiple JSON files, a JSON file name pattern must be used. The
patterns allow to use entity field values or parts of those in the file name
generation, making it possible to store one entity by JSON file, grouping
entities with common values in a same JSON, use specific subdirectories
depending on field values and possibly more. Patterns are specified using curly
braces and support a substring notation just like the PHP substr function:
`{offset[,length]:field_name}`
For example, the file name pattern `{-4:id}/{0,3:id}.json` used on the entity 
with the identifier "12345678_ABCD" will generate the sub-path "ABCD/123.json".
Following this file name pattern, the entity "12390000_ABCD" would be stored in
the same JSON file while the entity "12301234_EFGH" would be stored in
"EFGH/123.json" (same file name but in a different directory).

Since complex file name patterns can be used, the system needs a way to locate
the appropriate JSON file for a given entity when only its identifier is
provided while other fields are used in the pattern but are not available. In
order to solve that, a dedicated index file is required. It will store the
association between an entity identifier and its corresponding file, one by
line, separated by a tab character. It is only required when complex pattern are
used or when the user wants to explicitely specify a given JSON file for a given
entity rather than using the default one provided by the file name or name
pattern setting. Therefore, any entry for a given entity in the index file, once
there, will remain unchanged by the system.

Finally, it is possible to order and limit the number of entity field that will
be recorded in a JSON file. You can either pre-create the JSON file with only
the column name line or use the setting "List of entity field names to store in
JSON files". It may be convenient to restrict the number of field saved,
especially when using the xnttmulti module that can combine fields in an entity
from several external sources other than the JSON file.

REQUIREMENTS
------------

This module requires the following modules:

 * [External Entities](https://www.drupal.org/project/external_entities)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.
 * If you enabled the module using Drush, you must also rebuild the cache.

CONFIGURATION
-------------

The module has no menu or modifiable global settings. There is no configuration.
When enabled, the module will add a new storage client for external entity
types. Then, when you create a new external entity type, you can select the
"JSON files" plugin and have access to settings specific to the new external
entity.

MAINTAINERS
-----------

Current maintainers:
 * Valentin Guignon (guignonv) - https://www.drupal.org/u/guignonv
