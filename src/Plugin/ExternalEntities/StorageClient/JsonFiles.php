<?php

namespace Drupal\xnttjson\Plugin\ExternalEntities\StorageClient;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\external_entities\Plugin\ExternalEntities\StorageClient\FileClientBase;

/**
 * External entities storage client for JSON files.
 *
 * @StorageClient(
 *   id = "xnttjson",
 *   label = @Translation("JSON Files"),
 *   description = @Translation("Retrieves and stores records in JSON files.")
 * )
 */
class JsonFiles extends FileClientBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    TranslationInterface $string_translation,
    LoggerChannelFactoryInterface $logger_factory,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    ConfigFactory $config_factory,
    MessengerInterface $messenger,
    CacheBackendInterface $cache
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $string_translation,
      $logger_factory,
      $entity_type_manager,
      $entity_field_manager,
      $config_factory,
      $messenger,
      $cache
    );
    // Defaults.
    $this->fileType = 'JSON';
    $this->fileTypePlural = 'JSON';
    $this->fileTypeCap = 'JSON';
    $this->fileTypeCapPlural = 'JSON';
    $this->fileExtensions = ['.json'];
  }

  /**
   * {@inheritdoc}
   */
  protected function parseFile(string $file_path) :array {
    $data = [];
    $idf = $this->getSourceIdFieldName() ?? 0;

    // Extract field values from file path.
    $path_data = [];
    $structure = $this->configuration['structure'];
    if (FALSE !== strpos($structure, '{')) {
      $path_data = $entity = $this->getValuesFromPath($file_path);
    }

    $json_raw = file_get_contents($file_path);
    if (!empty($json_raw)) {
      $json = json_decode($json_raw, TRUE);
      if (NULL == $json) {
        $this->logger->error(
          'Failed to parse JSON file "'
          . $file_path
          . '": '
          . json_last_error_msg()
        );
      }
      else {
        if ($this->configuration['multi_records']) {
          $columns = [];
          // Multiple records, we assume we have an array.
          if (is_array($json)) {
            // Process each entity.
            foreach ($json as $entity) {
              if (!empty($entity[$idf])) {
                $data[$entity[$idf]] = $entity + $path_data;
              }
              $columns += array_flip(array_keys($entity));
            }
          }
          $data[''] = array_keys($columns);
        }
        else {
          // Single record. We assume we have one entity.
          if (is_array($json)) {
            // Set a default id value for invalid mappings.
            $id = $json[$idf] ?? $path_data[$idf] ?? current($json);
            $data[$id] = $json + $path_data;
            $data[''] = array_keys($json);
          }
        }
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  protected function generateRawData(array $entities_data) :string {
    // Don't save columns.
    $columns = array_flip($entities_data['']);
    unset($entities_data['']);
    if ($this->configuration['multi_records']) {
      // Multiple values.
      $json_entities = [];
      foreach ($entities_data as $id => $entity) {
        $json_entities[] = array_intersect_key($entity, $columns);
      }
      $json_raw = json_encode($json_entities);
    }
    else {
      // Single value.
      $entity = array_shift($entities_data);
      $json_entity = array_intersect_key($entity, $columns);
      $json_raw = json_encode($json_entity);
    }
    // Write records.
    return $json_raw;
  }

}
